/* 
 * File 	: ./index.js
 * Author(s)	: Zidmann
 * Function 	: Ce fichier est un exemple pour illustrer l'utilisation du module NPM 
 * Version  	: 1.0.0
 */

// Retrieve
var MongoClient = require('mongodb').MongoClient;

// Connect to the db
db = null;
require('mongodb').MongoClient.connect('mongodb://127.0.0.1:27017/formation_mongo',
	function(err, myDb){
	  	if (err){
	  		console.log('Could not connect to database');
	  	}
	  	else{
	  		db = myDb;
	  		console.log('Established connection to database');
			insertOnDB();
	  	}
	});

function insertOnDB(callback){
	db.collection('secret').insert({message: "Hello Word"}, {w : 0},
					function(err, res) {
						console.log('Err='+err);
						console.log('Res='+res[0].message);
					});
}

